package com.devcamp.task5830jpacustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5830JpaCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5830JpaCustomerApplication.class, args);
	}

}
