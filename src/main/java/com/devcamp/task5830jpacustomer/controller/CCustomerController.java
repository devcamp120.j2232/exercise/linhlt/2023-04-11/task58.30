package com.devcamp.task5830jpacustomer.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5830jpacustomer.model.CCustomer;
import com.devcamp.task5830jpacustomer.repository.CustomerRepository;

@RestController
@CrossOrigin
public class CCustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers(){
        try {
            List<CCustomer> customerList = new ArrayList<CCustomer>();
            customerRepository.findAll().forEach(customerList::add);
            if (customerList.size() ==0){
                return new ResponseEntity<>(customerList, HttpStatus.NOT_FOUND);
            }
            else return new ResponseEntity<>(customerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}



