package com.devcamp.task5830jpacustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5830jpacustomer.model.CCustomer;

public interface CustomerRepository extends JpaRepository<CCustomer, Long>{
    
}
